package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"gopkg.in/routeros.v2"
)

var (
	address  = flag.String("address", os.Getenv("MIKROTIK_HOST"), "Address")
	username = flag.String("username", os.Getenv("MIKROTIK_USER"), "Username")
	password = flag.String("password", os.Getenv("MIKROTIK_PASS"), "Password")
	interval = flag.Duration("interval", 0, "Interval")
	timeout  = flag.Duration("timeout", 10*time.Second, "http ipv6 detection timeout")
	addrlist = flag.String("addrlist", os.Getenv("MIKROTIK_ADDRLIST"), "Name of address list to sync")
)

const (
	entryCommentPrefix = "created by mikrotik-dyn-fw at "
)

type addressListEntry struct {
	ID      string
	Address string
}

func addressListGet(c *routeros.Client, listName string) ([]addressListEntry, error) {
	reply, err := c.Run("/ipv6/firewall/address-list/print", "?list="+listName, "=.proplist=.id,address")
	if err != nil {
		return nil, err
	}

	resp := make([]addressListEntry, len(reply.Re))
	for i, re := range reply.Re {
		resp[i].ID = re.Map[".id"]
		resp[i].Address = re.Map["address"]
	}

	return resp, nil
}

func addressListRemove(c *routeros.Client, id string) error {
	_, err := c.Run("/ipv6/firewall/address-list/remove", "=.id="+id)
	return err
}

func addressListAdd(c *routeros.Client, listName, address, comment string) error {
	_, err := c.Run("/ipv6/firewall/address-list/add", "=list="+listName, "=address="+address, "=comment="+comment, "=.proplist=.id,address")
	return err
}

func syncAddr(c *routeros.Client, listName, addr string) error {
	entries, err := addressListGet(c, listName)
	if err != nil {
		return fmt.Errorf("cant get address list %s: %w", listName, err)
	}

	log.Printf("got %d entries in list %s", len(entries), listName)

	entryFound := false
	for _, entry := range entries {
		if entry.Address == addr {
			log.Printf("entry %s: has correct addr, entryFound set", entry.ID)
			entryFound = true
			continue
		}

		log.Printf("entry %s: has incorrect addr (%s), removing", entry.ID, entry.Address)

		err = addressListRemove(c, entry.ID)
		if err != nil {
			return fmt.Errorf("cant remove entry %s (%s): %w", entry.ID, entry.Address, err)
		}
	}

	if !entryFound {
		log.Println("entry not found, creating one")
		err := addressListAdd(c, listName, addr, entryCommentPrefix+time.Now().String())
		if err != nil {
			return fmt.Errorf("cant create entry: %w", err)
		}
	} else {
		log.Println("entry already exists, not creating")
	}

	return nil
}

func checkIP6(c *http.Client) (string, error) {
	// has to be a service without A records (for ex api6.ipify.org cant be used)
	rsp, err := c.Get("https://v6.ident.me")
	if err != nil {
		return "", err
	}
	defer rsp.Body.Close()

	buf, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return "", err
	}

	return string(bytes.TrimSpace(buf)), nil
}

func runSync(routerC *routeros.Client, httpC *http.Client) {
	ip, err := checkIP6(httpC)
	if err != nil {
		log.Fatal("cant get ipv6:", err)
	}
	log.Printf("IPv6 is %s", ip)

	err = syncAddr(routerC, *addrlist, ip+"/128")
	if err != nil {
		log.Fatal("cant syncAddr:", err)
	}
}

func main() {
	flag.Parse()

	routerC, err := routeros.Dial(*address, *username, *password)
	if err != nil {
		log.Fatal("cant connect to mikrotik:", err)
	}

	var runonce bool
	var ticker *time.Ticker

	if *interval == time.Duration(0) {
		runonce = true
	} else {
		ticker = time.NewTicker(*interval)
	}

	httpC := &http.Client{
		Timeout: *timeout,
	}

	runSync(routerC, httpC)

	if runonce {
		os.Exit(0)
	}

	for range ticker.C {
		runSync(routerC, httpC)
	}
}
